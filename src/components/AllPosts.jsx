import React, { Component } from 'react';
import { Col} from 'react-bootstrap';

import { connect } from 'react-redux';

import Post from './Posts';

import EditComponent from './EditComponent';

class AllPost extends Component {
    render() {
        return (
            <Col md={12} className="app-allpost-container">
                <h1>Posts</h1>
                <hr/>
                {this.props.posts.map((post) => (
                    <div key={post.id}>
                        {post.editing ? <EditComponent post={post} key={post.id} /> :
                            <Post key={post.id} post={post} />}
                    </div>
                ))}
            </Col>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        posts: state
    }
}
export default connect(mapStateToProps)(AllPost);
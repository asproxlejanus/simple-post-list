import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Col, Button, Form, FormGroup, FormLabel } from 'react-bootstrap';


class PostForm extends Component {

  handleSubmit = (e) => {
    e.preventDefault();
    const title = this.getTitle.value;
    const message = this.getMessage.value;
    const data = {
      id: new Date(),
      title,
      message,
      editing: false
    }
    this.props.dispatch({
      type: 'ADD_POST',
      data
    });
    this.getTitle.value = '';
    this.getMessage.value = '';
  }
  render() {
    return (
      <Col className="app-post-container">
        <Form onSubmit={this.handleSubmit}>
          <h1>Nuevo Post</h1>
          <hr/>
          <FormGroup>
            <FormLabel inpt="nameInput">Titulo</FormLabel>
            <input className="form-control" required type="text" ref={(input) => this.getTitle = input}
              placeholder="Introduzca el título del post" />
          </FormGroup>
          <FormGroup>
            <FormLabel inpt="nameInput">Mensaje</FormLabel>
            <textarea className="form-control" required rows="5" ref={(input) => this.getMessage = input} cols="28"
              placeholder="Introduzca el contenido del post" />
          </FormGroup>
          <Button type="submit" variant="success">Post</Button>
        </Form>
      </Col>
    );
  }
}
export default connect()(PostForm);
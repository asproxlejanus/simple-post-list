import React, { Component } from 'react';
import { Col, Button, Row, Form } from 'react-bootstrap';
import { connect } from 'react-redux';


class EditComponent extends Component {
    handleEdit = (e) => {
        e.preventDefault();
        const newTitle = this.getTitle.value;
        const newMessage = this.getMessage.value;
        const data = {
            newTitle,
            newMessage
        }
        this.props.dispatch({ type: 'UPDATE', id: this.props.post.id, data: data })
    }
    render() {
        return (
            <Col>
                <Form onSubmit={this.handleEdit}>
                    <input className="form-control" required type="text" ref={(input) => this.getTitle = input}
                        defaultValue={this.props.post.title} placeholder="Enter Post Title" /><br /><br />
                    <textarea className="form-control" required rows="5" ref={(input) => this.getMessage = input}
                        defaultValue={this.props.post.message} cols="28" placeholder="Enter Post" /><br /><br />
                    <Button type="submit" variant="success">Update</Button>
                </Form>
            </Col>
        );
    }
}
export default connect()(EditComponent);
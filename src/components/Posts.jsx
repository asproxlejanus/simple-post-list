import React, { Component } from 'react';
import { Col, Button, Row } from 'react-bootstrap';
import { connect } from 'react-redux';

class Post extends Component {
    render() {
        return (
            <Col className="app-post-box-container">
                <Row className="app-new-post-animation">
                    <Col md={12} className="app-post-poststitle">
                        <h4>{this.props.post.title}</h4>
                        <p>{this.props.post.message}</p>
                    </Col>
                    <Col md={12} className="app-post-postsbuttons">
                        <Button variant="success" onClick={() => this.props.dispatch({ type: 'EDIT_POST', id: this.props.post.id })}>
                        <i className="fas fa-edit"></i>
                        </Button>
                        <Button variant="success" onClick={() => this.props.dispatch({ type: 'DELETE_POST', id: this.props.post.id })}>
                        <i className="far fa-trash-alt"></i>
                        </Button>
                    </Col>
                </Row>
            </Col>
        );
    }
}
export default connect()(Post);
import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import PostForm from './components/PostForm';
import AllPost from './components/AllPosts';

//IMPORTAMOS ESTILOS
import './styles/Main.css'


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  render() {
    return (
      <Container>
        <Col className="app-container" md={10}>
          <Row >
            <Col md={5}>
              <PostForm />
            </Col>
            <Col>
              <AllPost />
            </Col>
          </Row>
        </Col>
      </Container>
    );
  }
}

export default App;